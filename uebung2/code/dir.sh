dirPrint() {
	# Find all files and print them first
	local dir=$1
	local indent=$2
	for f in "$dir"/*; do
		if ! [ -d "$f" ]; then
			echo "${indent}File: $(pwd)$(echo "$f" | cut -c2-)"
		fi
	done

	# Find all folders and print them and their subs
	for f in "$dir"/*; do
		if [ -d "$f" ]; then
			echo "${indent}Directory: $(pwd)$(echo "$f" | cut -c2-)"
			dirPrint "$f" "\t$indent"
		fi
	done
}
if [ $# -eq 0 ]; then
	dirPrint . ""
else
	dirPrint "$1" ""
fi