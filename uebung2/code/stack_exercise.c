#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

/**
 * Struktur, die Eintraege des Stacks kapselt.
 */
typedef struct StackNode
{
    // Zeiger auf den Command-String
    char *command;
    // Zeiger auf den naechsten Knoten des Stacks
    struct StackNode *next_node;
} StackNode;

/**
 * Erstellt einen neuen Knoten des Stacks und haengt ihn am Anfang an
 * Gibt den neuen Startknoten zurück.
 */
StackNode *newNode(char *command)
{
    // allokiere neuen Speicherplatz für den naechsten Knoten auf dem Heap
    StackNode *node = (StackNode *)malloc(sizeof(StackNode));
    node->command = command;
    node->next_node = NULL;
    return node;
}

/**
 * Teste, ob Stack leer ist
 */
bool isEmpty(StackNode *root)
{
    // Falls root auf NULL zeigt, ist der Stack leer
    return !root;
}

/**
 * Fuege einen neuen Knoten dem Stack hinzu.
 */
void push(StackNode **pointer2root, char *command)
{
    // YOUR SOLUTION GOES HERE ...
    StackNode *new;
    new = newNode(command); // Create new stack node with the given command
    if (isEmpty(*pointer2root)) 
    {
    	*pointer2root = new; // If stack is empty then root point at new node
	}
    else { // If stack is not empty 
	    new->next_node = *pointer2root; // next_node of new node will point at current top node
	    *pointer2root = new; // root will point at new node
	}
}

/**
 * Gib den obersten Knoten zurueck und loesche diesen vom Stack (inklusive Speicherplatzfreigabe)
 */
bool pop(StackNode **pointer2root, char **pointer2command)
{
    // YOUR SOLUTION GOES HERE ...
    if (isEmpty(*pointer2root)) return false; // If stack is empty then return false
    else { // If stack is not empty
    	StackNode *nextNode;
    	nextNode = (*pointer2root)->next_node; // Store next node of top node 
    	*pointer2command = (*pointer2root)->command; // Get the command (aka pop out the top node)
    	free(*pointer2root); // Free memory of the top node
    	*pointer2root = nextNode; // Current top node will the the next node 
    }	
}

int main()
{
    StackNode *root = NULL;

    push(&root, "create: grades.txt");
    push(&root, "add: Max Mustermann 1.0");
    push(&root, "delete: grades.txt");

    char *command;

    while (pop(&root, &command))
    {
        printf("'%s' popped from stack \n", command);
    }

    printf("Stack is empty. Exit \n");

    return 0;
}