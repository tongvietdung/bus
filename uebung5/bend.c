#define _GNU_SOURCE

#include <sys/types.h>
#include <unistd.h>
#include <semaphore.h>
#include <sys/mman.h>
#include <stdio.h>
#include <stdint.h>
#include <signal.h>
#include <stdlib.h>
#include <time.h>
#include <sys/stat.h>
#include <fcntl.h>


void fahreAufPlattform(pid_t wagenNummer) {
    fprintf(stdout, "Wagen %d fährt auf Plattform\n", wagenNummer);
}

void oeffneTueren(pid_t wagenNummer) {
    fprintf(stdout, "Wagen %d öffnet die Türen\n", wagenNummer);
}

void schliesseTueren(pid_t wagenNummer) {
    fprintf(stdout, "Wagen %d schließt die Türen\n", wagenNummer);
}

void fahreVonPlattform(pid_t wagenNummer) {
    fprintf(stdout, "Wagen %d verlässt die Plattform\n", wagenNummer);
}

sem_t* 	verfuegbar;
sem_t* 	besitzt;
sem_t*	wagen_lock;
void AnkunftWagen() {
    pid_t wagen = getpid();
    
    fprintf(stdout, "Wagen %d kommt an\n", wagen);
    
    // aufplattform fahren wenn frei
    sem_wait(wagen_lock);
    fahreAufPlattform(wagen);
    
    oeffneTueren(wagen);

    // nutzer einsteigen lassen
    sem_post(verfuegbar);
    sem_post(verfuegbar);
    // besucher einsteigen
    sem_wait(besitzt);
    sem_wait(besitzt);
    // türen schließen wenn 2 Besucher drin
    schliesseTueren(wagen);
    
    fahreVonPlattform(wagen);
    sem_post(wagen_lock);
}



void betrete_wagen(int besucherNummer) {
    fprintf(stdout, "Besucher %d steigt in Wagen\n", besucherNummer);
}

sem_t* besucher_lock;
uint32_t* besucher_nummer;

void AnkunftBesucher() {
    uint32_t besucher;
    
    
    sem_wait(besucher_lock);
    besucher = (*besucher_nummer)++;
    fprintf(stdout, "Besucher %d kommt an\n", besucher);
    sem_post(besucher_lock);
    
    
    // warte auf freien sitzplatz
    sem_wait(verfuegbar);
    betrete_wagen(besucher);
    sem_post(besitzt);
}




int shouldEnd = 0;

/* SIGINT signal handler */
void signal_handler(int signalNum) {
    if(signalNum == SIGINT) {
        printf("Ok, schalte ab!\n");
        shouldEnd = 1;
    }
}


int main(void) {
    
#ifdef USE_NAMED_SEMAPHORES
    
    // zahlen die mit 0 starten werden octal interpretiert :)
    besucher_lock = sem_open("/besucher_lock", O_CREAT|O_EXCL, 0644, 1); /* Note O_EXCL */
    sem_unlink("/besucher_lock");
    verfuegbar = sem_open("/verfuegbar", O_CREAT|O_EXCL, 0644, 1); /* Note O_EXCL */
    sem_unlink("/verfuegbar");
    besitzt = sem_open("/besitzt", O_CREAT|O_EXCL, 0644, 1); /* Note O_EXCL */
    sem_unlink("/besitzt");
    wagen_lock = sem_open("/besitzt", O_CREAT|O_EXCL, 0644, 1); /* Note O_EXCL */
    sem_unlink("/besitzt");
    
#else
    /* wir müssen die semaphore mit anderen prozessen teilen
     * durch ein fork erhält der andere Prozess nur eine Kopie
     * wir müssen aber auf den selben Daten arbeiten...
     *
     * Wir nutzen mmap um uns geteilten speicher zu besorgen
     * auf dem wir lesen und schreiben dürfen
     * MAP_ANONYMOUS sorgt dafür dass wir nicht assoziierten
     * speicher bekommen also keine Datei oder so mappen
     * müssen einfach HEAP memory
     */
    
    besucher_lock = mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE,
                         MAP_ANONYMOUS | MAP_SHARED, 0, 0);
    if (!besucher_lock) {
        perror("out of memory\n");
        exit(1);
    }
    // initialisiere semaphore mit 1
    if(sem_init(besucher_lock, 1, 1) == -1) {
        perror("Semaphore init\n");
        exit(1);
    }
    

    verfuegbar	= mmap(NULL, sizeof(sem_t), PROT_READ| PROT_WRITE,
    					MAP_ANONYMOUS | MAP_SHARED, 0, 0);
    if (!verfuegbar) {
        perror("out of memory\n");
        exit(1);
    }
    // initialisiere semaphore mit 2
    if(sem_init(verfuegbar, 1, 0) == -1) {
        perror("Semaphore init\n");
        exit(1);
    }

    besitzt 	= mmap(NULL, sizeof(sem_t), PROT_READ| PROT_WRITE,
    					MAP_ANONYMOUS | MAP_SHARED, 0, 0);
    if (!besitzt) {
        perror("out of memory\n");
        exit(1);
    }
    // initialisiere semaphore mit 1
    if(sem_init(besitzt, 1, 0) == -1) {
        perror("Semaphore init\n");
        exit(1);
    }

    wagen_lock = mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE,
                         MAP_ANONYMOUS | MAP_SHARED, 0, 0);
    if (!wagen_lock) {
        perror("out of memory\n");
        exit(1);
    }
    // initialisiere semaphore mit 1
    if(sem_init(wagen_lock, 1, 1) == -1) {
        perror("Semaphore init\n");
        exit(1);
    }

#endif
    
    
    // und noch geteilten speicher um besuchernummern zu vergeben
    besucher_nummer = mmap(NULL, sizeof(uint32_t), PROT_READ | PROT_WRITE,
                           MAP_ANONYMOUS | MAP_SHARED, 0, 0);
    // der 1. besucher ist natürlich nummer0
    *besucher_nummer = 0;
    
    
    /* catch interrupts */
    signal(SIGINT, signal_handler);
    
    srand(time(NULL));
    
    // main loop 
    while (!shouldEnd) {
        
        // wagen und besucher erzeugen
        
        
        
        int car_or_user = rand();
        
        pid_t f_pid = fork();
        if (f_pid == 0) {
            
            /* Kinder sollen nicht auf SIG_INT hören */
            signal(SIGINT, SIG_DFL);
            
            /* wird das ein wagen oder ein besucher? */
            if(car_or_user < (RAND_MAX/2)) {
                // sagen wir mal das hier ist ein wagen
                
                AnkunftWagen();
            }else {
                // dann ist das ein besucher
                AnkunftBesucher();
            }
            // und tschüss
            exit(0);
        }else {
            // Im Vater
            sleep(1);
        }
        
    }
    
    
    
    
#ifdef USE_NAMED_SEMAPHORES
    sem_close(besucher_lock);
    
    
#else
    // semaphore töten
    sem_destroy(besucher_lock);
    sem_destroy(besitzt);
    sem_destroy(verfuegbar);
    sem_destroy(wagen_lock);
    // dran denken den speicher auch wieder abzugeben
    munmap(besucher_lock, sizeof(sem_t));
    munmap(wagen_lock, sizeof(sem_t));
    munmap(verfuegbar, sizeof(sem_t));
    munmap(besitzt, sizeof(sem_t));
    munmap(besucher_nummer, sizeof(uint32_t));
    
    
#endif

    
    
}

