#include <stdio.h>

int main(void)
{
	/* Create an array of miles value*/
	int miles[11];
	for (int i = 0; i <= 10; i++)
	{
		miles[i] = i*2;
	}

    double k = (double)1609 / (double)1000; /* Gleitkommakonstante zum Umrechnen */
    double kilometer;

    /* Calculate and print out the table */
    printf("Miles\t|\tKilometers\n");
	for (int i = 0; i <= 10; i++)
	{
		kilometer = k * miles[i];
		printf("%i\t|\t%6.2lf\n", miles[i], kilometer);
	}
    return 0;
}
