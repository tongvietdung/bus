#include <stdio.h>

int main(void)
{
    double k = (double)1609 / (double)1000; /* Gleitkommakonstante zum Umrechnen */
    
    printf("Entfernung in Meilen: ");
    double meilen; /* Gleitkommavariable */
    scanf("%lf", &meilen);

    double kilometer = k * meilen;

    printf("...ergibt in Kilometer ca: %6.2lf\n", kilometer);

    return 0;
}
