#include <stdio.h>
#include <limits.h>

/* Diese Funktion vergleicht und sortiert die beiden Feldeintraege
   "i1" und "i2" im Array "array".
 */

/* Because we don't use global variables, function exchange has to have array and minPt parameters
   to pass in the current array we want to sort and current value of min. 
 */
void exchange(int i1, int i2, int array[10], int* minPt) 
{
    if (array[i1] < array[i2])
    {
        if (array[i1] < *minPt)
            *minPt = array[i1];
    }
    else
    {
        if (array[i2] < *minPt)
            *minPt = array[i2];
        int tmp = array[i1];
        array[i1] = array[i2];
        array[i2] = tmp;
    }
}

int main()
{
    int min = INT_MAX;
    int array[10] = {4, 6, 2, 0, 9, 1, 5, 7, 8, 3};
    int anz = sizeof(array) / sizeof(*array);

    for (int i = 0; i < anz-1; ++i)
    {
        for (int j = i + 1; j < anz; ++j)
            exchange(i, j, array, &min);
    }

    printf("Die Zahlen in sortierter Reihenfolge:");
    for (int i = 0; i < anz; ++i)
        printf(" %d", array[i]);

    printf("\nDas Minimum: %i\n", min);

    return 0;
}