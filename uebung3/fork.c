#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>

int main()
{
    if (fork() == 0)
    {
        if (fork() == 0)
            printf("B \n");
        else
            printf("u \n");
        printf("S \n");
    }
    else
    {
        fork();
        fork();
        printf("20 \n");
        if (fork() == 0)
            printf("Zwan \n");
        else
            printf("Zig \n");
    }

    // Command wait wartet nur bis eine von den Kindprozesse terminiert ist. Es gibt pid von terminierten Kindprozessen aus, oder
    // -1 wenn es keine Kindprozesse gibt.
    // Die Codezeilen bedeutet, dass solange es gibt Kindprozesse, wartet der Root-Prozess.
    pid_t wpid;
    int status = 0;
    while ((wpid = wait(&status)) > 0);

    return 0;
}
