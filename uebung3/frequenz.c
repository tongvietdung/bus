#include <stdio.h>
#include <unistd.h>

int main(int argc, char const *argv[])
{
	// Total written character
	unsigned long total = 0;
	// Amount of character A
	unsigned long countA = 0;
	// Amount of character B
	unsigned long countB = 0;
	// Amount of character C
	unsigned long countC = 0;

	char c;

	while (1) {
		c = getchar();
		switch (c) {
			case 'A' :
				total++;
				countA++;
				fprintf(stdout, "B: %lu\tA: %lu\t C: %lu\n", countB, countA, countC);
				break;
			case 'B' :
				total++;
				countB++;
				fprintf(stdout, "B: %lu\tA: %lu\t C: %lu\n", countB, countA, countC);
				break;
			case 'C' :
				total++;
				countC++;
				fprintf(stdout, "B: %lu\tA: %lu\t C: %lu\n", countB, countA, countC);
				break;
		}
	}
	return 0;
}