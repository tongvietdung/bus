#include <stdio.h>
#include <unistd.h>


void child_A_proc()
{
  while (1)
  {
    fprintf(stdout, "%s", "A");
    fflush(stdout);
  
  }
}

void parent_proc()
{
  while (1)
  {
    write(1, "B", 1);
  }
}

void child_of_child_A_proc() 
{
  while (1)
  {
    fprintf(stdout, "%s", "C");
    fflush(stdout);
  }
}

int main(void)
{
  if (fork() == 0){
    if (fork() == 0)
    {
      child_of_child_A_proc();
    }
    else
      child_A_proc();
  }
  else
  {
    parent_proc();
  }
  return 0;
}
